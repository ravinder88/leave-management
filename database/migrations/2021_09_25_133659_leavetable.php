<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Leavetable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {

         Schema::create('leave', function (Blueprint $table) {
           $table->increments('id');
           $table->integer('user_id');
           $table->string('subject');
            $table->string('sender_email');
            $table->string('reciever_email');
            $table->date('start_date');
            $table->date('end_date');
            $table->string('description')->nullable();
            $table->string('department');
            $table->string('name');
            $table->string('leave_type');
            $table->string('status');
            $table->string('read_status')->nullable();

             $table->timestamps();
        });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::drop('leave');
     }
}
