<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Leave extends Model
{
  protected $table='leave';
    use HasFactory;
    protected $fillable = [
        'subject',
        'user_id',
        'sender_email',
        'reciever_email',
        'start_date',
        'end_date',
        'description',
        'department',
        'name',
        'leave_type',
        'status'
    ];
}
