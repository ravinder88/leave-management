<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Emails;

class EmailController extends Controller
{
    public function addEmail()
    {

      return view ('email/email');
    }
    public function storeEmail(Request $request){
      try {
          if(Emails::create($request->all())){
            return redirect()->route('listEmail' );
          }

    // Closures include ->first(), ->get(), ->pluck(), etc.
      } catch(\Illuminate\Database\QueryException $ex){
      dd($ex->getMessage());
      // Note any method of class PDOException can be called on $ex.
      }
  }
  public function listEmail()
  {
    //echo 'sss';exit;
    $emails=Emails::orderBy('id', 'DESC')->get()->toArray();

    return view('email/list',compact('emails'));
  }
}
