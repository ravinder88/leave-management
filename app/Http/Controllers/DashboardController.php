<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Leave;
use Auth;

class DashboardController extends Controller
{
    public function index()
    {
      $total_email=Leave::count();
      $unread_email=Leave::where('read_status',0)->count();
      $read_email=Leave::where('read_status',1)->count();

      if(Auth::user()->role=='user')
      {
        $total_email=Leave::where('user_id',Auth::id())->count();

        $unread_email=Leave::where('user_id',Auth::id())->where('read_status',0)->count();
        $read_email=Leave::where('user_id',Auth::id())->where('read_status',1)->count();
      }
      return view('dashboard/dashboard',compact('total_email','unread_email','read_email'));
    }
}
