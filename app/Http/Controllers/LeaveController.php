<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Leave;
use App\Models\User;
use App\Models\Emails;
use App\Lib\Email;
use Auth;
class LeaveController extends Controller
{
    //
    use Email;
    public function addLeave()
    {
      $emails = User::get()->toArray();
      return view('leave/add_leave',compact('emails'));
    }
    public function storeLeave(Request $request)
    {
      try {
          $name = $request->name;
          $emails = $request->input('reciever_email');
          $start_date = $request->input('start_date');
          $end_date = $request->input('end_date');
          $type = $request->input('leave_type');
          $subject = $request->input('subject');
          $inputs = $request->all();
          $inputs['reciever_email'] = implode(",",$emails);

          if(Leave::create($inputs)){
            $data = $this->email($name,$emails,$start_date,$end_date,$type,$subject);
            return redirect()->route('myLeave')->with($data['success'],$data['msg']);
          }
      } catch(\Illuminate\Database\QueryException $ex){
      dd($ex->getMessage());
      }
    }
    public function myLeave()
    {
      $leaves = Leave::where('user_id',Auth::id())->get()->toArray();
      return view('leave/my-leave',compact('leaves'));
    }
    public function listLeave()
    {
      $leaves = Leave::get()->toArray();
      return view('leave/list-leave',compact('leaves'));
    }
    public function replyTouser($id)
    {
      $data = Leave::where('id',$id)->get()->toArray();

      return view('leave/reply_to_user',compact('data'));
    }
    public function replyToStore(Request $request)
    {
      try {
          $id = $request->input('id');
          $email = $request->input('sender_email');
          $status = $request->input('status');
          $description = $request->input('description');
          $update_data['status'] = $status;
          if(Leave::where('id',$id)->update($update_data)){
            $data = $this->replyemail($email,$status,$description);
            return redirect()->route('listLeave' );
          }

      } catch(\Illuminate\Database\QueryException $ex){
      dd($ex->getMessage());
      }
    }
    public function deleteLeave(Request $request)
    {
      $id = $request->input('id');
      if(Leave::where('id',$id)->delete())
      {
        return redirect()->route('listLeave' );
      }
    }
}
