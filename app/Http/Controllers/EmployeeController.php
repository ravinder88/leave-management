<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\User;
use Auth;


class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('employee.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        return view('employee.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
              $input = $request->all(); 
              $input['password'] = bcrypt('123456');
              if(User::create($input)){
                return redirect()->route('employee.index')->with('success','Employee added successfully.');
              }

            } catch(\Illuminate\Database\QueryException $ex){
                dd($ex->getMessage());
              
            } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user,$id)
    {
        $useredit = $user->where('id',$id)->first();
        return view('employee.edit',compact('useredit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,User $user, $id)
    {
        $request->validate([
            'email' => 'required|unique:users,email,'.$id,
        ]);

        try{
            $user = $user->where('id',$id)->first();
            $user->update($request->all());
            return redirect()->route('employee.index')->with('success', 'employee updated successfully');
        } catch (\Exception $e) {
            dd($e->getMessage());
            return redirect()->route('employee.index')->with('error',$e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user,$id)
    {
        $user->destroy($id);
        return redirect()->route('employee.index')->with('success','Employee deleted successfully');
    }
}
