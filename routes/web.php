<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::group(['middleware' => 'auth'], function () {
Route::get('/dashboard', [App\Http\Controllers\DashboardController::class, 'index'])->name('dashboard');
Route::get('/add-email', [App\Http\Controllers\EmailController::class, 'addEmail'])->name('addEmail');
Route::post('/save-email', [App\Http\Controllers\EmailController::class, 'storeEmail'])->name('storeEmail');
Route::get('/email', [App\Http\Controllers\EmailController::class, 'listEmail'])->name('listEmail');
//Employee
 Route::resource('employee', \App\Http\Controllers\EmployeeController::class);

//leave
Route::get('/add-leave', [App\Http\Controllers\LeaveController::class, 'addLeave'])->name('addLeave');
Route::post('/save-leave', [App\Http\Controllers\LeaveController::class, 'storeLeave'])->name('storeLeave');
Route::get('/my-leave', [App\Http\Controllers\LeaveController::class, 'myLeave'])->name('myLeave');
Route::get('/leave-list', [App\Http\Controllers\LeaveController::class, 'listLeave'])->name('listLeave');
Route::get('/reply-to-user/{id}', [App\Http\Controllers\LeaveController::class, 'replyTouser'])->name('replyTouser');
Route::post('/reply-to-store', [App\Http\Controllers\LeaveController::class, 'replyToStore'])->name('replyToStore');
Route::post('/delete-leave', [App\Http\Controllers\LeaveController::class, 'deleteLeave'])->name('deleteLeave');
});
