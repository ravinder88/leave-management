@extends('layouts.dashboard_layout')
@section('content')
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="white-box">
                        <h3 class="box-title">List</h3>
                        <div class="table-responsive">
                            <table class="table text-nowrap">
                                <thead>
                                    <tr>
                                        <th class="border-top-0">#</th>
                                        <th class="border-top-0">Email</th>
                                        <th class="border-top-0">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  @if(!empty($emails))
                                      @foreach($emails as $email)
                                        <tr>
                                            <td>{{$email['id']}}</td>
                                            <td>{{$email['email']}}</td>
                                            <td><a href="">Edit</a>/<a href="">Delete</a></td>
                                        </tr>
                                        @endforeach
                                    @else
                                    <tr><td>No email Exist</td></tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection