@extends('layouts.dashboard_layout')
@section('content')
        <div class="page-breadcrumb bg-white">
            <div class="row align-items-center">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title"></h4>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="white-box">
                        <h3 class="box-title">Leave List</h3>
                        <div class="table-responsive">
                            <table class="table text-nowrap">
                                <thead>
                                    <tr>
                                        <th class="border-top-0">Subject</th>
                                        <th class="border-top-0">Sender</th>
                                        <th class="border-top-0">Name</th>
                                        <th class="border-top-0">Department</th>
                                        <th class="border-top-0">Start Date</th>
                                        <th class="border-top-0">End Date</th>
                                        <th class="border-top-0">Type</th>
                                        <th class="border-top-0">Status</th>
                                        <th class="border-top-0">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  @if(!empty($leaves))
                                  @foreach($leaves as $leave)
                                    <tr>
                                        <td>{{$leave['subject']}}</td>
                                        <td>{{$leave['sender_email']}}</td>
                                        <td>{{$leave['name']}}</td>
                                        <td>{{$leave['department']}}</td>
                                        <td>{{$leave['start_date']}}</td>
                                        <td>{{$leave['end_date']}}</td>
                                        <td>{{$leave['leave_type']}}</td>
                                        <td>{{$leave['status']}}</td>
                                        <td><a href="{{route('replyTouser',$leave['id'])}}">Reply</a> / <form class="inline-block" action="{{route('deleteLeave')}}" method="POST" onsubmit="return confirm(`Are you sure?`);">
                                            <input type="hidden" name="id" value="{{$leave['id']}}">
                                              @csrf
                                              <input type="submit" class="dropdown-item" value="Delete">
                                          </form></td>
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr><td>No leave Exist</td></tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection