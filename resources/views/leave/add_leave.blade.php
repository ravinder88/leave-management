@extends('layouts.dashboard_layout')

@section('content')
        <div class="page-breadcrumb bg-white">
            <div class="row align-items-center">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Add Leave</h4>
                </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <div class="d-md-flex">
                        <a href="{{route('addLeave')}}" target="_blank"
                            class="btn btn-danger  d-none d-md-block pull-right ms-3 hidden-xs hidden-sm waves-effect waves-light text-white">+Add Leave</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
          @include('flash-message')
            <div class="row">
                <div class="col-lg-12 col-xlg-9 col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <form class="form-horizontal form-material" method="post" id="add_email" action="{{route('storeLeave')}}">
                              @csrf
                              <input type="hidden" name="sender_email" value="{{Auth::user()->email}}">
                                <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                                <input type="hidden" name="name" value="{{Auth::user()->name}}">
                                <input type="hidden" name="status" value="Pending">
                                <div class="form-group mb-4">
                                    <label class="col-md-12 p-0">Subject</label>
                                    <div class="col-md-12 border-bottom p-0">
                                        <input   name="subject" type="text" placeholder="Enter Subject"
                                            class="form-control p-0 border-0" required> </div>
                                </div>
                                <div class="form-group mb-4">
                                    <label class="col-md-12 p-0">Start Date</label>
                                    <div class="col-md-12 border-bottom p-0">
                                        <input   name="start_date" type="date" placeholder="Enter start date"
                                            class="form-control p-0 border-0" required>
                                    </div>
                                </div>
                                <div class="form-group mb-4">
                                    <label class="col-md-12 p-0">End Date</label>
                                    <div class="col-md-12 border-bottom p-0">
                                        <input   name="end_date" type="date" placeholder="Enter start date"
                                            class="form-control p-0 border-0" required> </div>
                                </div>
                                <div class="form-group mb-4">
                                    <label class="col-sm-12 p-0">Select Type</label>
                                    <div class="col-sm-12 border-bottom">
                                        <select name="leave_type"class="form-select shadow-none p-0 border-0 form-control-line" required>
                                            <option value="Full Day">Full Day</option>
                                            <option value="Half Day">Half Day</option>
                                            <option value="Short Leave">Short Leave</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group mb-4">
                                    <label class="col-sm-12 p-0">Select Department</label>
                                    <div class="col-sm-12 border-bottom">
                                        <select name="department"class="form-select shadow-none p-0 border-0 form-control-line" required>
                                          <option value="Frontend Js">Frontend Js</option>
                                          <option value="Php">Php</option>
                                          <option value="Marketing">Marketing</option>
                                          <option value="Python">Python</option>
                                          <option value="Qa">Qa</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group mb-4">
                                    <label class="col-sm-12 p-0">Select Email</label>
                                    <div class="col-sm-12 border-bottom">
                                        <select name="reciever_email[]" class="form-select shadow-none p-0 border-0 form-control-line chosen-select" multiple   required>
                                          @foreach($emails as $email)
                                          <option value="{{$email['email']}}">{{$email['email']}}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group mb-4">
                                    <label class="col-md-12 p-0">Description</label>
                                    <div class="col-md-12 border-bottom p-0">
                                        <textarea name="description" rows="5" placeholder="optional" class="form-control p-0 border-0"></textarea>
                                    </div>
                                </div>
                                <div class="form-group mb-4">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-success">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
