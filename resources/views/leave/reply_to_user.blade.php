@extends('layouts.dashboard_layout')

@section('content')
        <div class="page-breadcrumb bg-white">
            <div class="row align-items-center">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Blank Page</h4>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-8 col-xlg-9 col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <form class="form-horizontal form-material" method="post" id="add_email" action="{{route('replyToStore')}}">
                              @csrf
                              <input type="hidden" name="id" value="{{$data[0]['id']}}">
                              <input type="hidden" name="sender_email" value="{{$data[0]['sender_email']}}">
                                <div class="form-group mb-4">
                                    <label class="col-sm-12 p-0">Select Status</label>
                                    <div class="col-sm-12 border-bottom">
                                        <select name="status"class="form-select shadow-none p-0 border-0 form-control-line" required>
                                            <option value="Approved">Approved</option>
                                            <option value="Rejected">Rejected</option>
                                            <option value="Waiting">Waiting</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group mb-4">
                                    <label class="col-md-12 p-0">Description</label>
                                    <div class="col-md-12 border-bottom p-0">
                                        <textarea name="description" rows="5" placeholder="optional" class="form-control p-0 border-0"></textarea>
                                    </div>
                                </div>
                                <div class="form-group mb-4">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-success">Reply</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
