@extends('layouts.dashboard_layout')
@section('content')
        <div class="page-breadcrumb bg-white">
            <div class="row align-items-center">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">My Leave</h4>
                </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <div class="d-md-flex">
                        <ol class="breadcrumb ms-auto">
                            <!-- <li><a href="#" class="fw-normal">+Add Leave</a></li> -->
                        </ol>
                        <a href="{{route('addLeave')}}" target="_blank"
                            class="btn btn-danger  d-none d-md-block pull-right ms-3 hidden-xs hidden-sm waves-effect waves-light text-white">+Add Leave</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <!-- Row -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="white-box">
                        <h3 class="box-title">My Leave</h3>
                        <div class="table-responsive">
                            <table class="table text-nowrap">
                                <thead>
                                    <tr>
                                        <th class="border-top-0">Subject</th>
                                        <th class="border-top-0">Start Date</th>
                                        <th class="border-top-0">End Date</th>
                                        <th class="border-top-0">Type</th>
                                        <th class="border-top-0">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  @if(!empty($leaves))
                                  @foreach($leaves as $leave)
                                    <tr>
                                        <td>{{$leave['subject']}}</td>
                                        <td>{{$leave['start_date']}}</td>
                                        <td>{{$leave['end_date']}}</td>
                                        <td>{{$leave['leave_type']}}</td>
                                        <td>{{$leave['status']}}</td>
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr><td>No leave Exists</td></tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
