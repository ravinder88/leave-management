@extends('layouts.dashboard_layout')
 
@section('content')
    <div class="row">
        <div class="col-lg-11 margin-tb">
             <div class="mx-auto md-8" id="btn_create">
                <a class="btn btn-success" href="{{ route('employee.create') }}"> Create</a>
            </div>
            <div class="pull-left">
                <h2>Employees List</h2>
            </div> 
        </div>
    </div>
    <table class=" col-lg-11 table table-bordered">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Email</th>
            <th>Role</th>
            <th>Department</th>
            <th>Action</th>
        </tr>
        @foreach ($users as $user)
        <tr>
            <td>{{ $user->id }}</td>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->role }}</td>
            <td>{{ $user->department }}</td>
            <td>
                <form action="{{ route('employee.destroy',$user->id) }}" method="POST" onsubmit="return confirm('Are you sure want to delete ?')">
                        @csrf
                        @method('DELETE')
                        <a class="btn btn-primary btn-bg" href="{{ route('employee.edit',$user->id) }}">Edit</a>
                        <button type="submit" class="btn btn-danger">Delete</button>
                </form> 
            </td>
        </tr>
        @endforeach 
    </table>      
@endsection