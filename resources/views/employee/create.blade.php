@extends('layouts.dashboard_layout')

@section('content')
        <div class="page-breadcrumb bg-white">
            <div class="row align-items-center">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Add Employee</h4>
                </div>
            </div>
        </div>
        <div class="container-fluid">
          @include('flash-message')
            <div class="row">
                <div class="col-lg-12 col-xlg-9 col-md-12">
                    <div class="card">
                        <div class="card-body">

                            <form class="form-horizontal form-material" method="post" id="add_email" action="{{route('employee.store')}}">
                              @csrf
                              <input type="hidden" name="sender_email" value="{{Auth::user()->email}}">
                                <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                                <input type="hidden" name="name" value="{{Auth::user()->name}}">
                                <input type="hidden" name="status" value="Pending">
                                <div class="form-group mb-4">
                                    <label class="col-md-12 p-0">Name</label>
                                        <div class="col-md-12 border-bottom p-0">
                                            <input  name="name" type="text" placeholder="Enter your name" class="form-control p-0 border-0" required> 
                                        </div>
                                </div>
                                <div class="form-group mb-4">
                                    <label class="col-md-12 p-0">Email</label>
                                        <div class="col-md-12 border-bottom p-0">
                                            <input   name="email" type="email" placeholder="Enter your email address" class="form-control p-0 border-0" required> 
                                        </div>
                                </div>
                                <div class="form-group mb-4">
                                    <label class="col-sm-12 p-0">Select Designation</label>

                                    <div class="col-sm-12 border-bottom">
                                        <select name="role"class="form-select shadow-none p-0 border-0 form-control-line" required>
                                            <option value="Team Leader">Team Leader</option>
                                            <option value="Employee">Employee</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group mb-4">
                                    <label class="col-sm-12 p-0">Select Department</label>

                                    <div class="col-sm-12 border-bottom">
                                        <select name="department"class="form-select shadow-none p-0 border-0 form-control-line" required>
                                          <option value="Frontend Js">Frontend Js</option>
                                          <option value="Php">Php</option>
                                          <option value="Marketing">Marketing</option>
                                          <option value="Python">Python</option>
                                          <option value="QA">QA</option>

                                        </select>
                                    </div>
                                </div>
                                <div class="form-group mb-4">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-success">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
