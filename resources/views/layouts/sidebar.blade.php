<aside class="left-sidebar" data-sidebarbg="skin6">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <!-- User Profile-->
                @php
                $actual_link = $_SERVER['REQUEST_URI'];
					$link=substr($actual_link, strrpos($actual_link, '/') + 1);

                @endphp
                <li class="text-center p-20 upgrade-btn">

                    <a href="{{route('dashboard')}}"
                        class="btn d-grid {{($link=='dashboard')?'active':''}}" target="_blank">
                        Dashboard</a>
                </li>
                @if(Auth::user()->role=='user')
                <li class="text-center p-20 upgrade-btn ">
                    <a class="btn d-grid {{($link=='add-leave')?'active':''}}" href="{{route('addLeave')}}"
                        aria-expanded="false">
                        Add leave
                    </a>
                </li>
                @endif
                @if(Auth::user()->role=='user')
                <li class="text-center p-20 upgrade-btn">

                    <a href="{{route('myLeave')}}"
                        class="btn d-grid {{($link=='my-leave')?'active':''}}" target="_blank">
                        My leave</a>
                </li>
                @endif
                @if(Auth::user()->role=='admin')
                {{-- <li class="text-center p-20 upgrade-btn">

                        <a href="{{route('addEmail')}}"
                            class="btn d-grid {{($link=='add-email')?'active':''}}" target="_blank">
                            Add Reciever Email</a>

                </li> --}}
                {{-- <li class="text-center p-20 upgrade-btn">

                    <a href="{{route('listLeave')}}"
                        class="btn d-grid {{($link=='leave-list')?'active':''}}" target="_blank">
                        Leave List</a>

                </li> --}}
                {{-- <li class="text-center p-20 upgrade-btn">

                    <a href="{{route('listEmail')}}"
                        class="btn d-grid {{($link=='email')?'active':''}}" target="_blank">
                        List Email</a>

                </li> --}}
                <li class="text-center p-20 upgrade-btn">

                        <a href="{{route('employee.index')}}"
                            class="btn d-grid">
                            Add Employee</a>

                </li>
                @endif

                <!-- <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="profile.html"
                        aria-expanded="false">
                        <i class="fa fa-user" aria-hidden="true"></i>
                        <span class="hide-menu">Profile</span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="basic-table.html"
                        aria-expanded="false">
                        <i class="fa fa-table" aria-hidden="true"></i>
                        <span class="hide-menu">Basic Table</span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="fontawesome.html"
                        aria-expanded="false">
                        <i class="fa fa-font" aria-hidden="true"></i>
                        <span class="hide-menu">Icon</span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="map-google.html"
                        aria-expanded="false">
                        <i class="fa fa-globe" aria-hidden="true"></i>
                        <span class="hide-menu">Google Map</span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="blank.html"
                        aria-expanded="false">
                        <i class="fa fa-columns" aria-hidden="true"></i>
                        <span class="hide-menu">Blank Page</span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="404.html"
                        aria-expanded="false">
                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                        <span class="hide-menu">Error 404</span>
                    </a>
                </li> -->
                <!-- <li class="text-center p-20 upgrade-btn">
                    <a href="https://www.wrappixel.com/templates/ampleadmin/"
                        class="btn d-grid btn-danger text-white" target="_blank">
                        Upgrade to Pro</a>
                </li> -->
            </ul>

        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
