<html>
<head>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css">
  <link href="{{ url('css/style.min.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ url('css/custom.css') }}" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="{{ url('css/chosen/style.css') }}">
  <link rel="stylesheet" href="{{ url('css/chosen/chosen.css') }}">
</head>
<body>
   <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full"
       data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
       @include('layouts.header')
       @include('layouts.sidebar')
       <div class="page-wrapper" style="min-height: 250px;">
           @yield('content')
         </div>
   </div>
@include('layouts.footer')
</body>
</html>
