
@extends('layouts.login_layout')

@section('content')
	<div class="container-fluid">
		<div class="row main-content bg-success text-center">
			<div class="col-md-4 text-center company__info">
				<span class="company__logo"><h2><span class="fa fa-android"></span></h2></span>
				<h4 class="company_title">Your Company Logo</h4>
			</div>
			<div class="col-md-8 col-xs-12 col-sm-12 login_form ">
				<div class="container-fluid">
					<div class="row">
						<h2>Log In</h2>
					</div>
					<div class="row">

              <form method="POST" action="{{ route('login') }}" class="form-group">
                  @csrf
							<div class="row">

                <input id="email" type="email" class="form__input form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
							</div>
							<div class="row">
								<!-- <span class="fa fa-lock"></span> -->
                <input id="password" type="password" class="form__input form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror</div>
							<div class="row">
                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                <label class="form-check-label" for="remember">
                    {{ __('Remember Me') }}
                </label>
							</div>
							<div class="row">
                <button type="submit" class="btn btn-primary">
                    {{ __('Login') }}
                </button>
							</div>
						</form>
					</div>
					<div class="row">
						<p>Don't have an account? <a href="/register">Register Here</a></p>
					</div>
				</div>
			</div>
		</div>

@endsection
